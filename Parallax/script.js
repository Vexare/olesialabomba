
//# Слои для параллакса
var ParallaxLayer = function( params ){

	if( !params )
		params = {};

	this.elem = document.createElement( "div" );
	this.elem.className = "parallax-layer";
	this.elem.style.backgroundImage = "url( '" + (params.src || "") + "' )";

	this.params = {
		force: params.force || 0.04,
		left: params.left || 0,
		top: params.top || 0,
		angle: params.angle,
		radians: 0,
		vector: { x: 0, y: 0 }
	};

	this.left( this.params.left );
	this.top( this.params.top );
	this.angle( this.params.angle );

};
ParallaxLayer.prototype = {
	left: function( value, limit, direction ){

		if( value === undefined )
			return this.params.left;

		this.params.left = value || 0;

		if( this.params.left > (limit) && direction < 0 ){
			this.params.left = -this.elem.clientWidth;
		}else if( this.params.left < (-this.elem.clientWidth) && direction > 0 ){
			this.params.left = limit;
		};

		this.elem.style.left = this.params.left + "px";

		return this;
	},
	top: function( value, limit, direction ){

		if( value === undefined )
			return this.params.top;

		this.params.top = value || 0;

		if( this.params.top > (limit) && direction < 0 ){
			this.params.top = -this.elem.clientHeight;
		}else if( this.params.top < (-this.elem.clientHeight) && direction > 0 ){
			this.params.top = limit;
		};

		this.elem.style.top = this.params.top + "px";

		return this;
	},
	force: function( value ){

		if( value === undefined )
			return this.params.force;

		this.params.force = value || 0;

		return this;
	},
	angle: function( value ){

		if( value === undefined )
			return this.params.angle;

		this.params.radians = this.params.angle * (3.14 / 180.0);
		this.params.vector = {
			x: Math.cos( this.params.radians ),
			y: Math.sin( this.params.radians )
		};

		return this;
	},
	vector: function(){
		return this.params.vector;
	},
	radians: function(){
		return this.params.radians;
	}
};
//^

//# Сам параллакс
var Parallax = function(){

	this.elem = document.createElement( "div" );
	this.elem.className = "parallax";
	this.layers = [];
	this.interval = null;
	this.deltaTime = 0;

};
Parallax.prototype = {
	add: function( params ){
		
		var layer = new ParallaxLayer( params );
		this.layers.push( layer );
		this.elem.appendChild( layer.elem );
		
		return this;
	},
	update: function(){

		var layer = null;
		var width = this.elem.clientWidth;
		var height = this.elem.clientHeight;

		for( var key in this.layers ){
			layer = this.layers[ key ];
			layer.left( layer.left() - layer.force() * -layer.vector().x * this.deltaTime, width, -layer.vector().x );
			layer.top( layer.top() - layer.force() * layer.vector().y * this.deltaTime, height, layer.vector().y );
		};

		return this;
	},
	animate: function( step ){

		this.deltaTime = step;
		var self = this;
		this.interval = setInterval(function(){
			self.update();
		}, step );

		return this;
	}
};
//^

var Ready = function(){
	
	var parallax = new Parallax();
	parallax.add({ src: "./clouds/cloud1.png", left: 0, top: 10, angle: 90, force: 0.22 });
	parallax.add({ src: "./clouds/cloud1.png", left: 600, top: 300, angle: 36, force: 0.32 });
	parallax.add({ src: "./clouds/cloud1.png", left: 600, top: 300, angle: 26, force: 0.22 });
	parallax.add({ src: "./clouds/cloud1.png", left: 600, top: 300, angle: 76, force: 0.12 });
	parallax.add({ src: "./clouds/cloud1.png", left: 600, top: 300, angle: 126, force: 0.52 });
	parallax.add({ src: "./clouds/cloud1.png", left: 600, top: 300, angle: 266, force: 0.08 });
	parallax.animate( 1000 / 60 );
	document.body.appendChild( parallax.elem );

};
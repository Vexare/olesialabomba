
var Characters = [
	{
		Health: 34,
		Armor: 31,
		Mana: 50,
		Attack: 35,
		Critical: 42
	},
	{
		Health: 14,
		Armor: 11,
		Mana: 20,
		Attack: 65,
		Critical: 22
	},
	{
		Health: 24,
		Armor: 41,
		Mana: 50,
		Attack: 55,
		Critical: 72
	},
	{
		Health: 74,
		Armor: 51,
		Mana: 20,
		Attack: 45,
		Critical: 72
	}
];

var BackColors = {
	Health: "stats-health",
	Armor: "stats-armor",
	Mana: "stats-mana",
	Attack: "stats-attack",
	Critical: "stats-critical"
};


var CharacterNames = [

	{	Name: "Venus  Stats"	},
	{	Name : "Zelda Stats"	},
	{	Name: "Nita Stats"	},
	{	Name: "Kara Stats"	}

];



var Widget = function(){

	this.wrap = document.createElement( "div");
	this.wrap.className = "wrapper";
	document.body.appendChild( this.wrap );

	this.bagCloud = document.createElement( "div");
	this.bagCloud.className = "bluround";
	this.wrap.appendChild( this.bagCloud );
	this.swarm = [ "1", "2", "3", "4" ];
	
	this.head = document.createElement( "div");
	this.head.className = "header";
	this.phead = document.createElement( "p");
	this.text = document.createTextNode("Select Hero");
	this.phead.appendChild( this.text );
	this.head.appendChild( this.phead );
	this.wrap.appendChild( this.head );

	this.elem = document.createElement( "div");
	this.elem.className = "select";
	this.wrap.appendChild( this.elem );

	this.hero = document.createElement( "div");
	this.hero.className = "hero";
	this.wrap.appendChild( this.hero );

	this.heroes = [ "1", "2", "3", "4" ];
	this.panel = document.createElement( "div" );
	this.panel.className = "stats";
	this.subview = document.createElement( "div" );
	this.subview.className = "subground";
	this.panel.appendChild( this.subview );


	this.topic();
	this.left();
	this.right();
	this.clicker();
	//this.para( 0 );
};

Widget.prototype = {

	left: function(){

		var self = this;

		for( var key in this.heroes ){

			(function(){
				var value = self.heroes[ key ];
				var transfer = document.createElement( "div" );
				transfer.className = "preview";
				transfer.style.backgroundImage = "url( './view/s" + ( value ) + ".png' )";
				transfer.addEventListener( "click", function(){
					self.press( value, Characters[ value - 1 ]);
			});
				self.elem.appendChild( transfer );
			})();
		};

		return this;
	},

	press: function( value, params ){ // value 1 - 4
		//console.log( value, params, this.elems );
		this.wrap.style.backgroundImage = "url( './image/bg" + ( value ) + ".jpg' )";
		this.hero.style.backgroundImage = "url( './image/pers" + ( value ) + ".png' )";

		for( var key in params ){
			//this.elems[ key + ":value" ].innerHTML = params[ key ];
			Range( this.elems[ key ].value, params[ key ]);
		}

		this.behalf.forename.innerHTML = CharacterNames[ value - 1 ].Name;

		return this;
	},

	right: function(){

		var hero = Characters[ 0 ];
		console.log( Characters, hero );

		this.elems = {};


		for( var key in hero ){

			this.elems[ key ] = {
				elem: document.createElement( "div" ),
				name:document.createElement( "div" ),
				title: document.createElement( "span" ),
				back:document.createElement( "div" ),
				value: document.createElement( "div" )
			};

			this.elems[ key ].elem.className = "small";

			this.elems[ key ].value.classList.add( BackColors[ key ] );
			this.elems[ key ].title.innerHTML = key;
			this.elems[ key ].elem.appendChild( this.elems[ key ].title );
			this.elems[ key ].back.appendChild(this.elems[ key ].value );
			this.elems[ key ].elem.appendChild( this.elems[ key ].back );

			this.subview.appendChild( this.elems[ key ].elem );

		};

		console.log( this.elems );
		this.press( this.heroes[ 2 ], Characters[ 0 ]);
		this.wrap.appendChild( this.panel );

		var x = 0;
		var y = 0;
		var deltaX = 0;
		var deltaY = 0;
		var positionX = 0;
		var positionY = 0;
		var positionBGX = 0;
		var positionBGY = 0;
		var first = true;
		var power = 0.07;

		var self = this;

		this.wrap.addEventListener( "mousemove", function( event ){

			if( first ){
				deltaX = 0;
				deltaY = 0;
				first = false;
			}else{
				deltaX = event.clientX - x;
				deltaY = event.clientY - y;
			};

			x = event.clientX;
			y = event.clientY;
			positionX -= deltaX * power;
			positionY -= deltaY * power;
			positionBGX += deltaX * power;
			positionBGY += deltaY * power;

			self.hero.style.backgroundPositionX = positionX + "px";
			self.wrap.style.backgroundPositionX = positionBGX + "px";
			//self.hero.style.backgroundPositionY = positionY + "px";
			//self.wrap.style.backgroundPositionY = positionBGY + "px";

		});

		return this;
	},

		clicker: function(){

		var self = this;
		this.behalf.button.addEventListener(
			"mousedown", function(){
				self.panel.style.height = "660px";
			}
		);
		this.panel.addEventListener(
			"mouseup", function(){
					self.panel.style.height= "0px";
			}
		);
	},

		topic: function(){

		this.behalf ={
			button: document.createElement( "div"),
			forename: document.createElement( "div" )
		};

		this.behalf.button.className = "toggle";

		this.behalf.button.appendChild(this.behalf.forename);
		this.wrap.appendChild(this.behalf.button);

		return this;
	}
/*
	para: function(){

		var  clo =this.swarm[0];
		this.cloudy ={};

		for( var key in clo ){

			this.cloudy[ key ] = {

				transition: document.createElement( "div" ),
				back:style.backgroundImage = "url( './clouds/cloud" + key  + ".png' )"
			};
			this.cloudy[ key ].transition = "blury";
			this.cloudy[ key ].transition.innerHTML = key;
			//this.cloudy[key].transition.
			this.bagCloud.appendChild(this.cloudy[key]);

		}


		
			return this;
		}
*/

};


var Range = function( item, val  ){
  	item.innerHTML = val;
	item.style.width = ( val/100 * 100 ) + "%";
};



/*
var Mode = function(){

	this.wrapper = document.createElement( "div" );
	this.wrapper.className = "body";
	document.body.appendChild( this.wrapper );

	this.enum = [ "1",  "2", "3",  "4" ];

   this.list();
	this.animated();
};
*/

/*
Mode.prototype = {

	list: function(){



		 this.first= document.createElement( "div" );
		 this.first.className = "clouds one";
		 this.wrapper.appendChild( this.first );

		 this.second= document.createElement( "div" );
		 this.second.className = "clouds two";
		 this.wrapper.appendChild( this.second );

		 this.third= document.createElement( "div" );
		 this.third.className = "clouds three";
		 this.wrapper.appendChild( this.third );

		 this.fourth= document.createElement( "div" );
		 this.fourth.className = "clouds four";
		 this.wrapper.appendChild( this.fourth );


			this.first.style.backgroundImage = "url( './clouds/cloud" + (  1 ) + ".png' )";
			this.second.style.backgroundImage = "url( './clouds/cloud" + ( 2  ) + ".png' )";
			this.third.style.backgroundImage = "url( './clouds/cloud" + ( 3 ) + ".png' )";
			this.fourth.style.backgroundImage = "url( './clouds/cloud" + ( 4  ) + ".png' )";

	//	}
		//return call;

		/*
		this.elems={};

		for( var key in this.enum ){

			this.elems[key] = {
				first: document.createElement( "div" ),
				second: document.createElement( "div" ),
				third: document.createElement( "div" ),
				fourth: document.createElement( "div" )
			};

			this.elems[key].first.className="one";
			this.elems[key].second.className="two";
			this.elems[key].third.className="three";
			this.elems[key].fourth.className="four";

			this.elems[key].first.style.backgroundImage="url( './clouds/cloud" + ( 1 ) + ".png' )";
			this.elems[key].second.style.backgroundImage="url( './clouds/cloud" + ( 2 ) + ".png' )";
			this.elems[key].third.style.backgroundImage="url( './clouds/cloud" + ( 3 ) + ".png' )";
			this.elems[key].fourth.style.backgroundImage="url( './clouds/cloud" + ( 4 ) + ".png' )";

			this.wrapper.appendChild( this.elems[key].first );
			this.wrapper.appendChild( this.elems[key].second  );
			this.wrapper.appendChild( this.elems[key].third  );
			this.wrapper.appendChild( this.elems[key].fourth  );


		};
*/


			/*
			var call = this.enum[ key ];
			var trans = document.createElement( "div" );
			trans.className = "pick";
			trans.style.backgroundImage ="url( './clouds/cloud" + (call) + ".png' )";

			this.wrapper.appendChild( trans );
			console.log( key );






	},

	animated:function(){

		var x = 0;
		var y = 0;
		var deltaX = 0;
		var deltaY = 0;
		var positionX = 0;
		var positionY = 0;
		var positionBGX = 0;
		var positionBGY = 0;
		var first = true;
		var power = 0.07;

		var self = this;
		var force = {
			first: 2.0,
			second: -1.0,
			third: 2.0,
			fourth: -1.0
		};
		var power = 17.0;
		var deltaTime = 60 / 1000;
		var left = 0;

//		this.interval = setInterval(function(){
//
//			left = (parseFloat( self.first.style.left || 0 ) + deltaTime * power * force.first);
//
//			self.first.style.left = left + "px";
//
//			left = (parseFloat( self.second.style.left || 0 ) + deltaTime * power * force.second);
//
//			self.second.style.left =  left + "px";
//
//			left = (parseFloat( self.third.style.left || 0 ) + deltaTime * power * force.third);
//
//			self.third.style.left =  left + "px";
//
//			left = (parseFloat( self.fourth.style.left || 0 ) + deltaTime * power * force.fourth);
//
//			self.fourth.style.left = left + "px";
//
//		}, 1000 / 60 );

		return this;

		this.wrapper.addEventListener( "mousemove", function( event ){

			if( first ){
				deltaX = 0;
				deltaY = 0;
				first = false;
			}else{
				deltaX = event.clientX - x;
				deltaY = event.clientY - y;
			};

			x = event.clientX;
			y = event.clientY;
			positionX -= deltaX * power;
			positionY -= deltaY * power;
			positionBGX += deltaX * power;
			positionBGY += deltaY * power;


			self.first.style.marginLeft = (positionX * force.first) + "px";
			self.second.style.marginLeft = (positionX * force.second) + "px";
			self.third.style.marginLeft = (positionX * force.third) + "px";
			self.fourth.style.marginLeft = (positionX * force.fourth) + "px";



			//self.wrapper.style.backgroundPositionX = positionBGX + "px";


			//self.hero.style.backgroundPositionY = positionY + "px";
			//self.wrap.style.backgroundPositionY = positionBGY + "px";

		});
	}

};
*/

var Ready = function(){
	var gadget = new Widget();
	/*var set =new SlidingPanel();*/
/*	var pick = new Mode();*/
};



































/*
 this.button = document.createElement( "div");
 this.button.className = "toggle";
 this.textButton =document.createTextNode("Hero Stats");
 this.button.appendChild(this.textButton);
 this.wrap.appendChild(this.button);
 */

/*
 clicker: function(){

 var self = this;
 this.button.addEventListener(
 "mousedown", function(){
 self.panel.style.height = "660px";
 }
 );
 this.panel.addEventListener(
 "mouseup", function(){
 self.panel.style.height= "0px";
 }
 );
 },

var SlidingPanel  = function(){

	var wrapper = document.createElement("div");
	wrapper.className = "imp";
	document.body.appendChild( wrapper );

	this.first = document.createElement("div");
	this.first.className = "hide";
	var text =document.createTextNode("Show the stats");
	this.first.appendChild( text );
	wrapper.appendChild( this.first);

	this.slide = document.createElement("div");
	this.slide.className = "block";
	wrapper.appendChild( this.slide);

	this.cross = document.createElement("div");
	this.cross.className = "crossing";
	wrapper.appendChild( this.cross);
	
	this.clicker();

};

SlidingPanel.prototype = {

	clicker: function(){

		var self = this;
		this.first.addEventListener(
			"mousedown", function(){
				 
				self.slide.style.height = "500px";
			}
		);

		this.cross.addEventListener(
			"mouseup", function(){
				 
				self.slide.style.height = "0px";
			}
		);
	}

}

*/


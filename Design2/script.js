var Characters = [
	{
		Power: 100,
		Stamina: 11,
		Armor: 11,
		Attack:  100,
		Damage: 100,
		Health: 100
	},
	{
		Power: 100,
		Stamina: 100,
		Armor: 100,
		Attack:  22,
		Damage: 22,
		Health: 100
	},
	{
		Power: 100,
		Stamina: 100,
		Armor: 33,
		Attack:  100,
		Damage: 33,
		Health: 100
	},
	{
		Power: 100,
		Stamina: 44,
		Armor: 100,
		Attack:  100,
		Damage: 44,
		Health: 100
	}
];

var Select = null;
var Push = function( unit ){

	Desc( unit );

	var element = document.getElementById( "listing" ).children[ unit ];

	if( Select ){
		Select.classList.remove( 'list-select' );
	}

	Select = element;
	element.classList.add( 'list-select' );

	var imperator = document.getElementById('imperator');
	var pers = document.getElementById('pers');

	imperator.style.backgroundImage = "url( './image/bg" + (unit + 1)+ ".jpg' )";
	pers.style.backgroundImage = "url( './image/pers" + (unit + 1)  + ".png' )";

};

var DescSelect = null;
var Desc = function( unit ){

	var part = document.getElementById( "descript" ).children[ unit ];

	if( DescSelect ){
		DescSelect.classList.remove( 'list-select' );
	}
	return;

	DescSelect = part;
	part.classList.add( 'list-select' );

};



var Ready = function(){

	Push( 1 );
	CreateStats();
	SelectHero();
};

var SelectHero = function( unit ){

	var hero = Characters[ unit ];

	for( var key in hero ){
		var slide = document.getElementById( key );
		Slide( slide, hero[ key] );
	}

};

var Slide = function( elem, value ){

	elem.innerHTML = value;
	elem.style.width = (value / 100 * 100) + "%";

};

var CreateStats = function(){
	
	var container = document.getElementById( "stats" );
	var hero = Characters[ 0 ];

	for( var key in hero ){
		var wrap = document.createElement( "div" );
		var title = document.createElement( "span" );
		var slide = document.createElement( "div" );
		title.innerHTML = key;
		Slide( slide, hero[ key ] );
		wrap.appendChild( title );
		wrap.appendChild( slide );
		wrap.id = key;
		container.appendChild( wrap );
	}
	
};



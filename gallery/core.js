var Images = [ "1.png", "2.jpg", "3.jpg", "4.jpg" ];
var Selected = 0;

var Select = function( index ){

	var limit = Images.length - 1;

	if( index > limit )
		index = 0;

	if( index < 0 )
		index = 0;

	var image = Images[ index ];

	if( !image )
		return;

	var element = document.getElementById( "image" );
	element.src = "./images/"+ image;

	var selectedElement = document.getElementById( "index" );
	selectedElement.innerHTML = index;

	Selected = index;

};

var Ready = function(){
	Select( 0 );
};

(function(){

	var string = "450 - steam\n600 - swtor\n7000 - spar\n2500 - fintess\n640 - to4ka\n300 - ff\n410 - ff\n650 - ff\n1000 - internet\n800 - telefone\n800 - ff\n320 - ff\n2200 - ff\n10000 - spar\n450 - steam\n1600 - 3ybi\n640 - ff\n600 - to4ka\n370 - ff\n370 - ff\n5000 - spar\n6000 - steam\n1800 - 3ybi\n370 - ff\n1100 - to4ka\n5400 - spar\n4000 - kamunal\n5500 - taxi";
	var list = string.match( /(.*)(.*\-)(.*)\n/g );
	var table = {};
	var total = 0;

	for( var key in list ){

		var line = list[ key ];
		var split = line.match( /(.*)\-(.*)/ ) || [];
		var key = (split[ 2 ] || "").trim();
		var value = parseInt( (split[ 1 ] || "").trim() ) || 0;

		if( !table[ key ] )
			table[ key ] = 0;

		table[ key ] += value;
		total += value;

	};

	console.log( table, total );

})();
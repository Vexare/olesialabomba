var Images = [ "a.jpg", "b.jpg", "c.jpg"]; //Images[ 0 ] = "a.jpg", Images[ 1 ] = "b.jpg",  Images[ 2 ] = "c.jpg";
var Selected = 0;

var Select = function( index ){

	var image = Images[ index ]; // var image = "b.jpg";

	if( !image )
		return false;

	var elems = document.getElementById( "image" );
	elems.src = "./images/" + image; // element.src = "./images/b.jpg";

	var indexElem = document.getElementById( "image-index" );
	indexElem.innerHTML = (index + 1) + "/" + Images.length;
    console.log( Images );
	return true;
};

var SelectNext = function(){
	Selected++;
	if( !Select( Selected ) )
		Selected--;
};

var SelectPrev = function(){
	Selected--;
	if( !Select( Selected ) )
		Selected++;
};

var Animate = function( elem, width, height, time ){

	var widthNow = elem.clientWidth;
	var heightNow = elem.clientHeight;
	var stepWidth = ((width - widthNow) / time) * 4;
	var stepHeight =  ((height - heightNow) / time) * 4;

	var interval = setInterval(function(){

		if( stepWidth < 0 ){

			if( widthNow <= width ){
				clearInterval( interval );
				return;
			};

		}else{

			if( widthNow >= width ){
				clearInterval( interval );
				return;
			};

		};

		widthNow += stepWidth;
		heightNow += stepHeight;
		elem.style.width = widthNow + "px";
		elem.style.height = heightNow + "px";

	}, 1 );

};
var Resize = function( event ){

	var elem = event.target;

	elem.style.width = 400 + "px";
	elem.style.height = 400 + "px";

};

var Render = function(){

	var list = document.getElementById("image-list");
    var key;

	for( key in Images ){
		var image = Images[ key ];
		var elem = document.createElement( "div" );
		elem.style.backgroundImage = "url( \"./images/" + image + "\" )";
		console.log( "url( \"./images/" + image + "\" )" );
		list.appendChild( elem );
		elem.onclick = Resize;
	};

};

var Ready = function(){
	Render();
	Select( 0 );
};











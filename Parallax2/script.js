var ParallaxLayer = function( params ){

	this.elem = document.createElement( "div" );
	this.elem.className = "parallax-layer";
	this.elem.style.backgroundImage = "url( '" + (params.src || "") + "' )";

};

ParallaxLayer.prototype = {};


var Parallax = function(){

	this.elem = document.createElement( "div" );
	this.elem.className = "parallax";
	this.layers = [];

};

Parallax.prototype = {

	add: function( params ){

		var layer = new ParallaxLayer( params );
		this.layers.push( layer  );
		this.elem.appendChild( layer.elem );

		return this;

	}

};





var Ready = function(){

	var parallax = new Parallax();
	parallax.add({ src: "./clouds/cloud1.png" });
	document.body. appendChild( parallax.elem );

};
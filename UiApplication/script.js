
var Show = function(){

	this.main = document.createElement( "div" );
	this.main.className = "main";
	document.body.appendChild( this.main );

	this.dof = document.createElement( "div" );
	this.dof.className = "dof";
	this.main.appendChild( this.dof );

	this.header = document.createElement( "div" );
	this.header.className = "header";
	this.main.appendChild( this.header );
	
	this.subHeader = document.createElement( "div" );
	this.subHeader.className = "sub-header";
	this.main.appendChild( this.subHeader );

	this.switch = document.createElement( "div" );
	this.switch.className = "switch";
	this.main.appendChild( this.switch );

	this.wrapper = document.createElement( "div" );
	this.wrapper.className = "wrapper";
	this.main.appendChild( this.wrapper );

	this.title = document.createElement( "div" );
	this.title.className = "title";
	this.wrapper.appendChild( this.title );

	this.text = document.createTextNode("Choose your hero");
	this.title.appendChild( this.text );

	this.heroPanel = document.createElement( "div" );
	this.heroPanel.className = "hero-panel";
	this.wrapper.appendChild( this.heroPanel );

	this.trialPanel = document.createElement( "div" );
	this.trialPanel.className = "trial-panel";
	this.main.appendChild( this.trialPanel );

	this.persona = document.createElement( "div" );
	this.persona.className = "persona";
	this.trialPanel.appendChild( this.persona );

	this.describe = document.createElement( "div" );
	this.describe.className = "describe";
	this.trialPanel.appendChild( this.describe );

	this.cross = document.createElement( "div" );
	this.cross.className = "cross";
	this.trialPanel.appendChild( this.cross );

	this.list = [ 0,  1, 2, 3, 4, 5, 6, 7, 8, 9 ];
	this.index = 0;

	var self = this;

	window.addEventListener( "resize", function(){

		if( document.body.offsetWidth < 860 ){
			self.shrink();
			self.box.style.display="none";
		}else if( document.body.offsetWidth > 860 ){
			self.header.style.display="block";
			self.header.style.width="100%";
			self.box.style.display="block";
		}
	});

	this.switch.addEventListener( "click", function(){
		self.effectHeader();
	});

	this.capital();
	this.clicker();
	this.cubeStats();
	this.clickCubeSkill();
	this.clickCubeWeapon();
	this.clickCubeStats();
	this.menuSelect();

};


Show.prototype = {

	menuSelect:function(){

		this.box = document.createElement( "div" );
		this.box.className = "box";
		this.header.appendChild( this.box );

		this.tekst = [ "Maps", "Shop","Chat Room","Collection","Loot Box" ];

		for( var key in this.tekst){

			var elem = this.tekst[ key ];
			var options = document.createElement( "div" );
			options.className = "point";
			options.innerHTML = elem;
			this.box.appendChild( options );

		}
		return this;
	},

	shrink: function(){
		this.header.style.width="0px";
		//this.blockText.innerHTML = "Open menu";
	},

	effectHeader: function(){

		if( this.header.clientWidth ){
			this.shrink();
			this.box.style.display="none";
		}else{
			this.header.style.width="320px";
			this.box.style.display="block";
		}
		return this;
	},

	capital: function(){

		var self = this;

		for( var key in this.list ){

			let value = this.list[ key ];
			var them = document.createElement( "div" );
			them.className = "page";

			var preview = document.createElement( "div" );
			preview.className = "preview";
			preview.style.backgroundImage = "url( './preview/s" + ( value + 1 ) + ".png' )";
			them.appendChild( preview );

			var callme = document.createElement( "div" );
			callme.className = "callme";
			callme.innerHTML = Stats[ key ].Name;
			them.appendChild( callme );

			them.addEventListener( "click", function(){
				self.press( value );
			});
			this.heroPanel.appendChild( them );
		};
		return this;
	},

	press: function( value ){

		this.persona.style.backgroundImage ="url( './full/pers" + ( value +1 ) + ".png' )";
		this.index = value;

		this.heroPanel.style.display = "none";
		this.trialPanel.style.display = "block";

		return this;
	},

	cubeStats: function(){

		this.cubeOne = document.createElement( "div" );
		this.cubeOne.className = "cub-one";
		this.trialPanel.appendChild( this.cubeOne );

		this.cubeTwo = document.createElement( "div" );
		this.cubeTwo.className = "cub-two";
		this.trialPanel.appendChild( this.cubeTwo );

		this.cubeThree = document.createElement( "div" );
		this.cubeThree.className = "cub-three";
		this.trialPanel.appendChild( this.cubeThree );

		this.cubeFour = document.createElement( "div" );
		this.cubeFour.className = "cub-four";
		this.trialPanel.appendChild( this.cubeFour );

		this.skills = document.createElement( "div" );
		this.skills.className = "skills";
		this.trialPanel.appendChild( this.skills );

		this.weapon = document.createElement( "div" );
		this.weapon.className = "weapon";
		this.trialPanel.appendChild( this.weapon );

		this.stats = document.createElement( "div" );
		this.stats.className = "stats";
		this.trialPanel.appendChild( this.stats );
	},

	clickCubeSkill: function(){

		var self = this;

		this.cubeOne.addEventListener( "click", function(){

			while( self.skills.firstChild )
				self.skills.removeChild( self.skills.firstChild );

			var set = Skills[ self.index ];

			for( var key in set ){

				let skill = set[ key ];
				var total =document.createElement( "div" );
				var wrap = document.createElement( "div" );
				var item = document.createElement( "div" );
				item.style.width = ( skill/100 * 100 ) + "%";
				wrap.innerHTML = key;
				item.innerHTML= skill;

				total.appendChild(wrap);
				total.appendChild(item);

				self.skills.appendChild( total );

			};

		});

		return this;
	},

	clickCubeWeapon: function(){

		var self = this;

		this.cubeTwo.addEventListener( "click", function(){

			while( self.weapon.firstChild )
				self.weapon.removeChild( self.weapon.firstChild );

			var wep = Weapon[ self.index ];

			for( var key in wep ){

				let weapon = wep[ key ];
				var total =document.createElement( "div" );
				var wrap = document.createElement( "div" );
				var item = document.createElement( "div" );
				item.style.width = ( weapon/100 * 100 ) + "%";
				wrap.innerHTML = key;
				item.innerHTML= weapon;

				total.appendChild(wrap);
				total.appendChild(item);

				self.weapon.appendChild( total );
			}
		});

		return this;
	},

	clickCubeStats: function(){

		var self = this;

		this.cubeThree.addEventListener( "click", function(){

			while( self.stats.firstChild )
				self.stats.removeChild( self.stats.firstChild );

			var state = Stats[ self.index ];

			for( var key in state ){

				let stats = state[ key ];
				var wrap = document.createElement( "div" );
				var item = document.createElement( "div" );

				if( key == "Name" ){
					item.innerHTML = "Name: " + stats;
				}else{
					item.style.width  = ( stats / 100 * 100 ) + "%";
				};

				wrap.appendChild( item );
				self.stats.appendChild( wrap );
			};
		});

		return this;
	},

	clicker: function(){

		var self = this;

		this.cross.addEventListener(
			"mouseup", function(){
				self.heroPanel.style.display = "block";
				self.trialPanel.style.display = "none";
			}
		);

		return this;
	}

};


var Ready = function(){

	var app = new Show();

};




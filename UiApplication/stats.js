var Stats = [

	{
		Name: "Lorenco",
		Health: 38,
		Armor: 31,
		Mana: 50,
		Attack: 35,
		Critical: 42
	},

	{
		Name: "Kora",
		Health: 14,
		Armor: 11,
		Mana: 20,
		Attack: 65,
		Critical: 22
	},

	{
		Name: "Luxor",
		Health: 24,
		Armor: 41,
		Mana: 50,
		Attack: 55,
		Critical: 72
	},

	{
		Name: "Mikki",
		Health: 74,
		Armor: 51,
		Mana: 20,
		Attack: 45,
		Critical: 72
	},

	{
		Name: "Rovena",
		Health: 34,
		Armor: 31,
		Mana: 50,
		Attack: 35,
		Critical: 42
	},

	{
		Name: "Leja",
		Health: 14,
		Armor: 11,
		Mana: 20,
		Attack: 65,
		Critical: 22
	},

	{
		Name: "Chao",
		Health: 24,
		Armor: 41,
		Mana: 50,
		Attack: 55,
		Critical: 72
	},

	{
		Name: "Linda",
		Health: 74,
		Armor: 51,
		Mana: 20,
		Attack: 45,
		Critical: 72
	},

	{
		Name: "Jexon",
		Health: 24,
		Armor: 41,
		Mana: 50,
		Attack: 55,
		Critical: 72
	},

	{
		Name: "Mercy",
		Health: 74,
		Armor: 51,
		Mana: 20,
		Attack: 45,
		Critical: 72
	}
];


var Selected = null;
var SelectCharacter = function( element, index ){
	
	Selected = element;
	Selected.classList.add( "list-selected" );

	var wrap = document.getElementById( "wrap" );
	var character = document.getElementById( "character" );

	wrap.style.backgroundImage = "url( './bg" + index + ".png' )";
	character.style.backgroundImage = "url( './char" + index + ".png' )";

};

var Ready = function(){

	SelectCharacter( document.getElementById( "list" ).childNodes[ 1 ], 1 );

};

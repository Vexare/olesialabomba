
var Jump = function(){

	this.main = document.createElement( "div" );
	this.main.className = "main";
	document.body.appendChild( this.main );

	this.header = document.createElement( "div" );
	this.header.className = "header";
	this.main.appendChild( this.header );

	this.lowerHeader = document.createElement( "div" );
	this.lowerHeader.className = "lower-header";
	this.main.appendChild( this.lowerHeader );

	this.button = document.createElement( "div" );
	this.button.className = "button";
	this.main.appendChild( this.button );

	this.blockText = document.createElement( "div" );
	this.blockText.className = "block-text";
	this.button.appendChild( this.blockText );

	this.buttonText = document.createTextNode( "Open menu" );
	this.blockText.appendChild( this.buttonText );

	this.panel();

	var self = this;

	window.addEventListener( "resize", function(){

		if( document.body.offsetWidth < 835 ){
			self.shrink();
			self.bar.style.display="none";
		}else if( document.body.offsetWidth > 830 ){
			self.header.style.display="block";
			self.header.style.width="100%";
			self.bar.style.display="block";
		}
	});

	this.button.addEventListener( "click", function(){
		self.effect();
	});

};


Jump.prototype ={

	panel:function(){

		this.bar = document.createElement( "div" );
		this.bar.className = "bar";
		this.header.appendChild( this.bar );

		this.user = document.createElement( "div" );
		this.user.className = "user";
		this.header.appendChild( this.user );

		this.userText = document.createTextNode( "My account" );
		this.user.appendChild( this.userText );

		this.list = [ "","", "", "", "" ];

		for( var key in this.list ){

			var date = this.list[ key ];
			var block = document.createElement( "div" );
			block.className="options";
			block.innerHTML = date;
			var text = document.createTextNode( "Hello!");
			block.appendChild( text );
			this.bar.appendChild( block );
		}

		return this;
	},

	shrink: function(){
		this.header.style.width="0px";
		this.blockText.innerHTML = "Open menu";
	},

	effect: function(){

		if( this.header.offsetWidth ){
			this.shrink();
			this.bar.style.display="none";
		}else{
			this.header.style.width="330px";
		//	this.header.style.display="block";
			this.bar.style.display="block";
			this.blockText.innerHTML = "Close menu";
		}
		return this;
	}
};

var Ready = function(){

	var get = new Jump();

};




